<?php

namespace LoanApp\Entity\LoanEntity;

use LoanApp\Entity\GuidEntity\GuidInterface;
use LoanApp\Entity\TrancheEntity\TrancheInterface;

class Loan implements LoanInterface
{
    private $guid;
    private $startDate;
    private $endDate;
    private $tranches;
    
    public function __construct(
         GuidInterface $guid,
         \DateTime $startDate,
         \DateTime $endDate
    ){
        $this->setGuid($guid->generateGuid());
        $this->setStartDate($startDate);
        $this->setEndDate($endDate);
        $this->tranches = [];
    }

    public function getGuid(): string
    {
        return $this->guid;
    }
    
    public function setGuid(string $guid)
    {
        $this->guid = $guid;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTime $startDate)
    {
        $this->startDate = $startDate;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTime $endDate)
    {
        $this->endDate = $endDate;
    }

    public function getTranches(): array
    {
        return $this->tranches;
    }

    public function addTranche(TrancheInterface $tranche)
    {
        $this->tranches[] = $tranche->getGuid();
    }
}