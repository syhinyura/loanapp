<?php

namespace LoanApp\Entity\LoanEntity;

use LoanApp\Entity\TrancheEntity\TrancheInterface;

interface LoanInterface
{
    public function getGuid(): string;
    public function addTranche(TrancheInterface $tranche);
    public function getTranches(): array;
}