<?php

namespace LoanApp\Entity\GuidEntity;

interface GuidInterface
{
    public function generateGuid(): string;
}