<?php

namespace LoanApp\Entity\GuidEntity;

class Guid implements GuidInterface
{
    public function generateGuid(): string
    {
        return uniqid(rand(1,10000));
    }
}