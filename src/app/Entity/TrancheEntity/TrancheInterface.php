<?php

namespace LoanApp\Entity\TrancheEntity;

use LoanApp\Entity\DepositEntity\DepositInterface;
use LoanApp\Lists\DepositListsInterface;

interface TrancheInterface
{
    public function getGuid(): string;
    public function getInterest(): float;
    public function getLoanGuid(): string;
    public function getPrice(): float;
    public function addDeposit(DepositInterface $deposit);
    public function getDeposits();
    public function getAllowPriceToInvest(DepositListsInterface $depositLists): float;
}
