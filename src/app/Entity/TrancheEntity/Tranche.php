<?php

namespace LoanApp\Entity\TrancheEntity;

use LoanApp\Entity\DepositEntity\DepositInterface;
use LoanApp\Entity\GuidEntity\GuidInterface;
use LoanApp\Entity\LoanEntity\LoanInterface;
use LoanApp\Lists\DepositListsInterface;

class Tranche implements TrancheInterface
{
    private $guid;
    private $interest;
    private $loan;
    private $price;
    private $deposits;

    public function __construct(GuidInterface $guid, float $interest, LoanInterface $loan, float $price)
    {
        $this->setGuid($guid->generateGuid());
        $this->setInterest($interest);
        $this->setLoan($loan);
        $this->setPrice($price);
        $this->deposits = [];
    }

    public function getGuid(): string
    {
        return $this->guid;
    }

    public function setGuid(string $guid)
    {
        $this->guid = $guid;
    }

    public function getInterest(): float
    {
        return $this->interest;
    }

    public function setInterest(float $interest)
    {
        $this->interest = $interest;
    }

    public function getLoanGuid(): string
    {
        return $this->loan;
    }

    public function setLoan(LoanInterface $loan)
    {
        $this->loan = $loan->getGuid();
    }
    
    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    public function addDeposit(DepositInterface $deposit)
    {
        $this->deposits[] = $deposit->getGuid();
    }

    public function getDeposits(): array
    {
        return $this->deposits;
    }

    public function getAllowPriceToInvest(DepositListsInterface $depositLists): float
    {
        $price = $this->getPrice();
        foreach($this->getDeposits() as $deposit){
            $depositPrice = $depositLists->getByGuid($deposit)->getAmount();
            if($depositPrice > 0){
                $price -= $depositPrice;
            }
        }
        return $price;
    }
}