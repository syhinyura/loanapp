<?php

namespace LoanApp\Entity\DepositEntity;

interface DepositInterface
{
    public function getGuid(): string;
    public function getDateCreate(): \DateTime;
    public function getAmount(): float;
    public function getTrancheGuid(): string;
    public function getInvestor(): string;
}