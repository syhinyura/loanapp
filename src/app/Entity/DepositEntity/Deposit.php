<?php

namespace LoanApp\Entity\DepositEntity;

use LoanApp\Entity\GuidEntity\GuidInterface;
use LoanApp\Entity\InvestorEntity\InvestorInterface;
use LoanApp\Entity\TrancheEntity\TrancheInterface;

class Deposit implements DepositInterface
{
    private $guid;
    private $dateCreate;
    private $amount;
    private $investor;
    private $tranche;

    public function __construct(
        GuidInterface $guid,
        float $amount,
        InvestorInterface $investor,
        TrancheInterface $tranche
    ){
        $this->setGuid($guid->generateGuid());
        $this->setAmount($amount);
        $this->setInvestor($investor);
        $this->setTranche($tranche);
        $this->setDateCreate(new \DateTime());
    }

    public function getGuid(): string
    {
        return $this->guid;
    }

    public function setGuid(string $guid)
    {
        $this->guid = $guid;
    }

    public function getDateCreate(): \DateTime
    {
        return $this->dateCreate;
    }

    public function setDateCreate(\DateTime $dateCreate)
    {
        $this->dateCreate = $dateCreate;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function setAmount(float $amount)
    {
        $this->amount = $amount;
    }

    public function getInvestor(): string
    {
        return $this->investor;
    }

    public function setInvestor(InvestorInterface $investor)
    {
        $this->investor = $investor->getGuid();
    }

    public function getTrancheGuid(): string
    {
        return $this->tranche;
    }

    public function setTranche(TrancheInterface $tranche)
    {
        $this->tranche = $tranche->getGuid();
    }
}