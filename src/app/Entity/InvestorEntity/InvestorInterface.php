<?php

namespace LoanApp\Entity\InvestorEntity;

use LoanApp\Entity\DepositEntity\DepositInterface;

interface InvestorInterface
{
    public function getGuid(): string;
    public function getWalletAmount(): float;
    public function addAmountToWallet(float $amount);
    public function withdrawFromWallet(float $amount);
    public function makeDeposit(DepositInterface $deposit);
    public function getDeposits(): array;
}