<?php

namespace LoanApp\Entity\InvestorEntity;

use LoanApp\Entity\DepositEntity\DepositInterface;
use LoanApp\Entity\GuidEntity\GuidInterface;

class Investor implements InvestorInterface
{
    private $guid;
    private $walletAmount;
    private $deposits;

    public function __construct(GuidInterface $guid)
    {
        $this->setGuid($guid->generateGuid());
        $this->setWalletAmount(0);
        $this->deposits = [];
    }

    public function getGuid(): string
    {
        return $this->guid;
    }

    public function setGuid(string $guid)
    {
        $this->guid = $guid;
    }

    public function getWalletAmount(): float
    {
        return $this->walletAmount;
    }

    public function setWalletAmount(float $walletAmount)
    {
        $this->walletAmount = $walletAmount;
    }

    public function addAmountToWallet(float $amount)
    {
        $this->walletAmount += $amount;
    }

    public function withdrawFromWallet(float $amount)
    {
        $this->walletAmount -= $amount;
    }

    public function makeDeposit(DepositInterface $deposit)
    {
        $this->deposits[] = $deposit->getGuid();
    }

    public function getDeposits(): array
    {
        return $this->deposits;
    }
}