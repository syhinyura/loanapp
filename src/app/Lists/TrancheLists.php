<?php

namespace LoanApp\Lists;

use LoanApp\Entity\TrancheEntity\TrancheInterface;

class TrancheLists extends SingletonLists implements TrancheListsInterface
{
    private $tranches;

    public function __construct()
    {
        $this->tranches = [];
    }
    
    public function getAll(): array
    {
        return $this->tranches;
    }

    public function add(TrancheInterface $tranche)
    {
        if(empty($this->getByGuid($tranche->getGuid()))){
            $this->tranches[] = $tranche;
        }else{
            throw new \Exception("Tranche exists in current TrancheList.");
        }
    }

    public function getByGuid($findGuid)
    {
        foreach($this->getAll() as $tranche){
            if($tranche->getGuid() === $findGuid){
                return $tranche;
            }
        }
        return [];
    }
}