<?php

namespace LoanApp\Lists;

use LoanApp\Entity\InvestorEntity\InvestorInterface;

class InvestorLists extends SingletonLists implements InvestorListsInterface
{
    private $investors;

    public function __construct()
    {
        $this->investors = [];
    }

    public function getAll(): array
    {
        return $this->investors;
    }

    public function add(InvestorInterface $investor)
    {
        if(empty($this->getByGuid($investor->getGuid()))){
            $this->investors[] = $investor;
        }else{
            throw new \Exception("Investor exists in current InvestorList.");
        }
    }

    public function getByGuid(string $findGuid)
    {
        foreach($this->getAll() as $investor){
            if($investor->getGuid() === $findGuid){
                return $investor;
            }
        }
        return [];
    }
}