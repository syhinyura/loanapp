<?php

namespace LoanApp\Lists;

use LoanApp\Entity\LoanEntity\LoanInterface;

class LoanLists extends SingletonLists implements LoanListsInterface
{
    private $loans;

    public function __construct()
    {
        $this->loans = [];
    }

    public function getAll(): array
    {
        return $this->loans;
    }

    public function add(LoanInterface $loan)
    {
        if(empty($this->getByGuid($loan->getGuid()))){
            $this->loans[] = $loan;
        }else{
            throw new \Exception("Loan exists in current LoanList.");
        }
    }

    public function getByGuid($findGuid)
    {
        foreach($this->getAll() as $loan){
            if($loan->getGuid() === $findGuid){
                return $loan;
            }
        }
        return [];
    }
    
    public function getByPeriod(\DateTime $dateFrom, \DateTime $dateTo)
    {
        $result = [];
        foreach($this->getAll() as $loan){
            if(
                $loan->getStartDate()->getTimestamp() <= $dateFrom->getTimestamp()
                && $loan->getStartDate()->getTimestamp() < $dateTo->getTimestamp()
                && $dateFrom->getTimestamp() < $dateTo->getTimestamp()
            ){
                $result[] = $loan;
            }
        }
        return $result;
    }
}