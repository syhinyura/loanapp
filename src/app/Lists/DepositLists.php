<?php

namespace LoanApp\Lists;

use LoanApp\Entity\DepositEntity\DepositInterface;

class DepositLists extends SingletonLists implements DepositListsInterface
{
    private $deposits;

    public function __construct()
    {
        $this->deposits = [];
    }

    public function getAll(): array
    {
        return $this->deposits;
    }

    public function add(DepositInterface $deposit)
    {
        if(empty($this->getByGuid($deposit->getGuid()))){
            $this->deposits[] = $deposit;
        }else{
            throw new \Exception("Deposit exists in current DepositList.");
        }
    }

    public function getByGuid($findGuid)
    {
        foreach($this->getAll() as $deposit){
            if($deposit->getGuid() === $findGuid){
                return $deposit;
            }
        }
        return [];
    }
}