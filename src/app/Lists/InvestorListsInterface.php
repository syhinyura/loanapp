<?php

namespace LoanApp\Lists;

use LoanApp\Entity\InvestorEntity\InvestorInterface;

interface InvestorListsInterface
{
    public function getAll(): array;
    public function add(InvestorInterface $investor);
    public function getByGuid(string $findGuid);
}