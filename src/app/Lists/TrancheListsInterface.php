<?php

namespace LoanApp\Lists;

use LoanApp\Entity\TrancheEntity\TrancheInterface;

interface TrancheListsInterface
{
    public function getAll(): array;
    public function add(TrancheInterface $tranche);
    public function getByGuid(string $findGuid);
}