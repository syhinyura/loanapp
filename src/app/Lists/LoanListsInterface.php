<?php

namespace LoanApp\Lists;

use LoanApp\Entity\LoanEntity\LoanInterface;

interface LoanListsInterface
{
    public function getAll(): array;
    public function add(LoanInterface $investor);
    public function getByGuid(string $findGuid);
}