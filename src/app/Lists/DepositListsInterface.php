<?php

namespace LoanApp\Lists;

use LoanApp\Entity\DepositEntity\DepositInterface;

interface DepositListsInterface
{
    public function getAll(): array;
    public function add(DepositInterface $deposit);
    public function getByGuid(string $findGuid);
}