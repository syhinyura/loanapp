<?php

namespace LoanApp\Controllers;

use LoanApp\Entity\DepositEntity\Deposit;
use LoanApp\Entity\DepositEntity\DepositInterface;
use LoanApp\Entity\GuidEntity\Guid;
use LoanApp\Lists\DepositListsInterface;
use LoanApp\Lists\InvestorListsInterface;
use LoanApp\Lists\LoanListsInterface;
use LoanApp\Lists\TrancheListsInterface;

class MakeDepositController
{
    private $investors;
    private $tranches;
    private $deposits;
    private $loans;

    public function __construct(
        InvestorListsInterface $investors,
        TrancheListsInterface $tranches,
        DepositListsInterface $deposits,
        LoanListsInterface $loans
    ){
        $this->investors = $investors;
        $this->tranches = $tranches;
        $this->deposits = $deposits;
        $this->loans = $loans;
    }

    public function invest(float $amountRequest, string $investorRequest, string $trancheRequest)
    {
        if($amountRequest <= 0){
            throw new \Exception("Price should be more than zero.");
        }

        $investor = $this->investors->getByGuid($investorRequest);
        if(!$investor){
            throw new \Exception("Investor does not exists.");
        }

        $tranche = $this->tranches->getByGuid($trancheRequest);
        if(!$tranche){
            throw new \Exception("Tranche does not exists.");
        }

        if($investor->getWalletAmount() < $amountRequest){
            throw new \Exception("Investor does not have enough money.");
        }

        $priceAllowToInvest = $tranche->getAllowPriceToInvest($this->deposits);
        if($priceAllowToInvest === 0.00){
            throw new \Exception("Tranche was sold");
        }elseif($priceAllowToInvest < $amountRequest){
            throw new \Exception("You can invest only ".$priceAllowToInvest);
        }

        $loan = $this->loans->getByGuid($tranche->getLoanGuid());
        if(!$loan){
            throw new \Exception("Loan does not exists.");
        }elseif(
            $loan->getStartDate()->getTimestamp() > time()
            || $loan->getEndDate()->getTimestamp() < time()
        ){
            throw new \Exception("Tranche does not active now.");
        }

        $deposit = new Deposit(new Guid(), $amountRequest, $investor, $tranche);
        $tranche->addDeposit($deposit);
        $this->deposits->add($deposit);
        return $deposit;
    }

    public function getProfit($filter = []){
        $resultProfit = [];
        $dateFrom = $filter["dateFrom"] ? $filter["dateFrom"] : new \DateTime('d.m.Y');
        $dateTo = $filter["dateTo"] ? $filter["dateTo"] : new \DateTime();
       
        $resultLoans = $this->loans->getByPeriod(
            $dateFrom->setTime(00, 00, 00),
            $dateTo->setTime(23, 59, 59)
        );
        
        foreach($resultLoans as $loan){
            foreach($loan->getTranches() as $tranche){
                $resultTranche = $this->tranches->getByGuid($tranche);
                foreach($resultTranche->getDeposits() as $deposit){
                     $resultDeposit = $this->deposits->getByGuid($deposit);
                     if($resultDeposit instanceof DepositInterface){
                        $datePeriod = new \DatePeriod(
                            $resultDeposit->getDateCreate()->setTime(00, 00, 00),
                            (new \DateInterval('P1D')),
                            $dateTo->setTime(23, 59, 59)
                        );
                        $dateGroupDays = [];
                        foreach($datePeriod as $period){
                            $dateGroupDays[$period->format("m-Y")]++;
                        }
                        foreach($dateGroupDays as $groupMonth => $groupDays){
                            $interestPerDaily = $resultTranche->getInterest() / 31;
                            $profit = $resultDeposit->getAmount() * (($interestPerDaily*$groupDays)/100);
                            $resultProfit[$groupMonth][$resultDeposit->getInvestor()][$resultTranche->getGuid()] += round($profit,2); 
                        }
                     }
                }
            }
        }
        return $resultProfit;
    }
}