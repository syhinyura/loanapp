<?php

namespace LoanAppTest\Controllers;

use LoanApp\Controllers\MakeDepositController;
use LoanApp\Entity\GuidEntity\Guid;
use LoanApp\Entity\InvestorEntity\Investor;
use LoanApp\Entity\LoanEntity\Loan;
use LoanApp\Entity\TrancheEntity\Tranche;
use LoanApp\Lists\DepositLists;
use LoanApp\Lists\InvestorLists;
use LoanApp\Lists\LoanLists;
use LoanApp\Lists\SingletonLists;
use LoanApp\Lists\TrancheLists;
use PHPUnit\Framework\TestCase;

class MakeDepositControllerTest extends TestCase
{
    private $loanLists;
    private $trancheLists;
    private $depositLists;
    private $investorLists;

    protected function setUp(): void
    {
        $this->loanLists = LoanLists::getInstance();
        $this->trancheLists = TrancheLists::getInstance();
        $this->depositLists = DepositLists::getInstance();
        $this->investorLists = InvestorLists::getInstance();
    }

    protected function fullListsWithTempData()
    {
        $loan = new Loan(new Guid(), new \DateTime('01.10.2015'), new \DateTime('15.11.2025'));
        $this->loanLists->add($loan);

        $tranche = new Tranche(new Guid(), 3, $loan, 1000);
        $tranche->setGuid("Tranche_1");
        $loan->addTranche($tranche);
        $this->trancheLists->add($tranche);

        $tranche = new Tranche(new Guid(), 6, $loan, 1000);
        $tranche->setGuid("Tranche_2");
        $loan->addTranche($tranche);
        $this->trancheLists->add($tranche);

        $investor = new Investor(new Guid());
        $investor->setGuid("Investor_1");
        $investor->addAmountToWallet(1000);
        $this->investorLists->add($investor);

        $investor = new Investor(new Guid());
        $investor->setGuid("Investor_2");
        $investor->addAmountToWallet(1000);
        $this->investorLists->add($investor);
    }

    public function tearDown(): void
    {
        $reflection = new \ReflectionClass(SingletonLists::class);
        $instance = $reflection->getProperty('instances');
        $instance->setAccessible(true);
        $instance->setValue(null, null);
        $instance->setAccessible(false);
        $this->loanLists = LoanLists::getInstance();
        $this->trancheLists = TrancheLists::getInstance();
        $this->depositLists = DepositLists::getInstance();
        $this->investorLists = InvestorLists::getInstance();
    }

    public function testInvestGetExceptionIfAmountZero()
    {
        $controller = new MakeDepositController(
            $this->investorLists,
            $this->trancheLists,
            $this->depositLists,
            $this->loanLists
        );
        try {
            $deposit = $controller->invest(0, "Investor_1", "Tranche_1");
            $this->assertTrue(false, "Exception does not happen.");
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), "Price should be more than zero.");
        }
    }

    public function testInvestGetExceptionIfInvestorDoesNotExist()
    {
        $this->fullListsWithTempData();
        $controller = new MakeDepositController(
            $this->investorLists,
            $this->trancheLists,
            $this->depositLists,
            $this->loanLists
        );
        try {
            $deposit = $controller->invest(10, "Investor_Not_Exist", "Tranche_1");
            $this->assertTrue(false, "Exception does not happen.");
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), "Investor does not exists.");
        }
    }

    public function testInvestGetExceptionIfTrancheDoesNotExist()
    {
        $this->fullListsWithTempData();
        $controller = new MakeDepositController(
            $this->investorLists,
            $this->trancheLists,
            $this->depositLists,
            $this->loanLists
        );
        try {
            $deposit = $controller->invest(10, "Investor_1", "Tranche_Not_Exist");
            $this->assertTrue(false, "Exception does not happen.");
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), "Tranche does not exists.");
        }
    }

    public function testInvestGetExceptionIfInvestorDoesNotHaveEnoughMoney()
    {
        $this->fullListsWithTempData();
        $controller = new MakeDepositController(
            $this->investorLists,
            $this->trancheLists,
            $this->depositLists,
            $this->loanLists
        );
        try {
            $deposit = $controller->invest(1001, "Investor_1", "Tranche_1");
            $this->assertTrue(false, "Exception does not happen.");
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), "Investor does not have enough money.");
        }
    }

    public function testInvestGetExceptionIfTrancheWasSold()
    {
        $this->fullListsWithTempData();
        $controller = new MakeDepositController(
            $this->investorLists,
            $this->trancheLists,
            $this->depositLists,
            $this->loanLists
        );
        try {
            $deposit = $controller->invest(1000, "Investor_2", "Tranche_1");
            $deposit = $controller->invest(1, "Investor_1", "Tranche_1");
            $this->assertTrue(false, "Exception does not happen.");
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), "Tranche was sold");
        }
    }

    public function testInvestGetExceptionIfTooMuchInvest()
    {
        $this->fullListsWithTempData();
        $controller = new MakeDepositController(
            $this->investorLists,
            $this->trancheLists,
            $this->depositLists,
            $this->loanLists
        );
        try {
            $deposit = $controller->invest(990, "Investor_2", "Tranche_1");
            $deposit = $controller->invest(100, "Investor_1", "Tranche_1");
            $this->assertTrue(false, "Exception does not happen.");
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), "You can invest only 10");
        }
    }

    public function testInvestGetExceptionIfLoanDoesNotActive()
    {
        $this->fullListsWithTempData();
        $loan = new Loan(new Guid(), new \DateTime('01.10.2015'), new \DateTime('15.11.2015'));
        $this->loanLists->add($loan);

        $tranche = new Tranche(new Guid(), 3, $loan, 1000);
        $tranche->setGuid("Tranche_3");
        $loan->addTranche($tranche);
        $this->trancheLists->add($tranche);

        $controller = new MakeDepositController(
            $this->investorLists,
            $this->trancheLists,
            $this->depositLists,
            $this->loanLists
        );
        try {
            $deposit = $controller->invest(1000, "Investor_1", "Tranche_3");
            $this->assertTrue(false, "Exception does not happen.");
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), "Tranche does not active now.");
        }
    }

    public function testInvestGetDeposits()
    {
        $this->fullListsWithTempData();
        $controller = new MakeDepositController(
            $this->investorLists,
            $this->trancheLists,
            $this->depositLists,
            $this->loanLists
        );
        $controller->invest(100, "Investor_1", "Tranche_1");
        $controller->invest(300, "Investor_2", "Tranche_1");
        $controller->invest(200, "Investor_1", "Tranche_1");
        $controller->invest(400, "Investor_2", "Tranche_1");
        $this->assertEquals(4, count($this->depositLists->getAll()));
    }

    public function testInvestGetProfit()
    {
        $this->fullListsWithTempData();
        $controller = new MakeDepositController(
            $this->investorLists,
            $this->trancheLists,
            $this->depositLists,
            $this->loanLists
        );
        $deposit = $controller->invest(1000, "Investor_1", "Tranche_1");
        $deposit->setDateCreate(new \DateTime("03.10.2015"));

        $deposit = $controller->invest(500, "Investor_2", "Tranche_2");
        $deposit->setDateCreate(new \DateTime("10.10.2015"));

        $profit = $controller->getProfit([
            "dateFrom" => new \DateTime('01.10.2015'),
            "dateTo" => new \DateTime('05.11.2015')
        ]);
        $this->assertEquals(28.06,  $profit['10-2015']['Investor_1']['Tranche_1']);
        $this->assertEquals(21.29,  $profit['10-2015']['Investor_2']['Tranche_2']);

        $profit = $controller->getProfit([
            "dateFrom" => new \DateTime('01.10.2015'),
            "dateTo" => new \DateTime('04.10.2015')
        ]);
        // 3 / 31 / 100 = 0.000967 - percent per day
        //0.000967*1000 = 0.9677 - profit per day
        //0.9677*2 = 1.935 - profit by period
        $this->assertEquals(1.94,  $profit['10-2015']['Investor_1']['Tranche_1']);
    }
}