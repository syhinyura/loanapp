<?php

namespace LoanAppTest\EntityTest;

use LoanApp\Entity\DepositEntity\DepositInterface;
use LoanApp\Entity\GuidEntity\GuidInterface;
use LoanApp\Entity\LoanEntity\LoanInterface;
use LoanApp\Entity\TrancheEntity\Tranche;
use LoanApp\Lists\DepositListsInterface;
use PHPUnit\Framework\TestCase;

class TrancheTest extends TestCase
{
    protected $guid;
    protected $startDate;
    protected $endDate;
    protected $tranche;

    protected function setUp(): void
    {
        $this->guid = $this->createMock(GuidInterface::class);
        $this->loan = $this->createMock(LoanInterface::class);
        $this->deposit = $this->createMock(DepositInterface::class);
        $this->depositList = $this->createMock(DepositListsInterface::class);
    }

    public function testAttributes()
    {
        $this->assertClassHasAttribute('guid', Tranche::class);
        $this->assertClassHasAttribute('interest', Tranche::class);
        $this->assertClassHasAttribute('loan', Tranche::class);
        $this->assertClassHasAttribute('price', Tranche::class);
        $this->assertClassHasAttribute('deposits', Tranche::class);
    }

    public function testCheckConstructor()
    {
        $this->guid
            ->method('generateGuid')
            ->willReturn("guid_1");
        $this->loan
            ->method('getGuid')
            ->willReturn("guid_2");

        $tranche = new Tranche($this->guid, 10, $this->loan, 100.5);
        $this->assertEquals("guid_1", $tranche->getGuid());
        $this->assertEquals(10, $tranche->getInterest());
        $this->assertEquals("guid_2", $tranche->getLoanGuid());
        $this->assertEquals(100.5, $tranche->getPrice());
        $this->assertEquals([], $tranche->getDeposits());
    }

    public function testAddDeposit()
    {
        $this->deposit
            ->method('getGuid')
            ->willReturn("guid_3");
        $tranche = new Tranche($this->guid, 20, $this->loan, 200.5);
        $tranche->addDeposit($this->deposit);
        $this->assertEquals(1, count($tranche->getDeposits()));
        $this->assertEquals('guid_3', $tranche->getDeposits()[0]);
    }

    public function testGetAllowPriceToInvest()
    {
        $tranche = new Tranche($this->guid, 2, $this->loan, 200.5);
        $this->assertEquals(200.5, $tranche->getAllowPriceToInvest($this->depositList));
        $tranche->addDeposit($this->deposit);
        $this->depositList
            ->method('getByGuid')
            ->willReturn($this->deposit);
        $this->deposit
            ->method('getAmount')
            ->willReturn(1.1);
        $this->assertEquals(199.4, $tranche->getAllowPriceToInvest($this->depositList));
    }
}