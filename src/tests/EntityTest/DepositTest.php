<?php

namespace LoanAppTest\EntityTest;

use LoanApp\Entity\DepositEntity\Deposit;
use LoanApp\Entity\GuidEntity\GuidInterface;
use LoanApp\Entity\InvestorEntity\InvestorInterface;
use LoanApp\Entity\TrancheEntity\TrancheInterface;
use PHPUnit\Framework\TestCase;

class DepositTest extends TestCase
{
    protected $guid;
    protected $startDate;
    protected $endDate;
    protected $tranche;

    protected function setUp(): void
    {
        $this->guid = $this->createMock(GuidInterface::class);
        $this->investor = $this->createMock(InvestorInterface::class);
        $this->tranche = $this->createMock(TrancheInterface::class);
    }

    public function testCheckConstructor()
    {
        $this->guid
            ->method('generateGuid')
            ->willReturn("guid_deposit");
        $this->investor
            ->method('getGuid')
            ->willReturn("guid_investor");
        $this->tranche
            ->method('getGuid')
            ->willReturn("guid_tranche");
            
        $deposit = new Deposit($this->guid, 100, $this->investor, $this->tranche);    
        $this->assertEquals("guid_deposit", $deposit->getGuid());
        $this->assertEquals(100, $deposit->getAmount());
        $this->assertEquals('guid_investor', $deposit->getInvestor());
        $this->assertEquals('guid_tranche', $deposit->getTrancheGuid());
    }
}