<?php

namespace LoanAppTest\EntityTest;

use LoanApp\Entity\DepositEntity\DepositInterface;
use LoanApp\Entity\GuidEntity\GuidInterface;
use LoanApp\Entity\InvestorEntity\Investor;
use PHPUnit\Framework\TestCase;

class InvestorTest extends TestCase
{
    protected $guid;
    protected $startDate;
    protected $endDate;
    protected $tranche;

    protected function setUp(): void
    {
        $this->guid = $this->createMock(GuidInterface::class);
        $this->deposit = $this->createMock(DepositInterface::class);
    }

    public function testAttributes()
    {
        $this->assertClassHasAttribute('guid', Investor::class);
        $this->assertClassHasAttribute('walletAmount', Investor::class);
        $this->assertClassHasAttribute('deposits', Investor::class);
    }

    public function testCheckConstructor()
    {
        $this->guid
            ->method('generateGuid')
            ->willReturn("guid_1");
        $investor = new Investor($this->guid);
        $this->assertEquals("guid_1", $investor->getGuid());
        $this->assertEquals(0, $investor->getWalletAmount());
        $this->assertEquals([], $investor->getDeposits());
    }

    public function testCanAddDepositToInvestor()
    {
        $investor = new Investor($this->guid);
        $this->deposit
            ->method('getGuid')
            ->willReturn("guid_2");
        $investor->makeDeposit($this->deposit);
        $this->assertEquals(1, count($investor->getDeposits()));
        $this->assertEquals("guid_2", $investor->getDeposits()[0]);
    }

    public function testAddAmountToWalletAndWithdraw()
    {
        $investor = new Investor($this->guid);
        $this->assertEquals(0, $investor->getWalletAmount());
        $investor->addAmountToWallet(10);
        $investor->addAmountToWallet(40);
        $this->assertEquals(50, $investor->getWalletAmount());
        $investor->withdrawFromWallet(24);
        $this->assertEquals(26, $investor->getWalletAmount());
    }
}