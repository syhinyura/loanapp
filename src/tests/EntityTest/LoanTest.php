<?php

namespace LoanAppTest\EntityTest;

use LoanApp\Entity\GuidEntity\GuidInterface;
use LoanApp\Entity\LoanEntity\Loan;
use LoanApp\Entity\TrancheEntity\TrancheInterface;
use PHPUnit\Framework\TestCase;

class LoanTest extends TestCase
{
    protected $guid;
    protected $startDate;
    protected $endDate;
    protected $tranche;

    protected function setUp(): void
    {
        $this->guid = $this->createMock(GuidInterface::class);
        $this->startDate = new \DateTime("01.07.2021");
        $this->endDate = new \DateTime("15.08.2021");
        $this->tranche = $this->createMock(TrancheInterface::class);
    }

    public function testAttributes()
    {
        $this->assertClassHasAttribute('guid', Loan::class);
        $this->assertClassHasAttribute('startDate', Loan::class);
        $this->assertClassHasAttribute('endDate', Loan::class);
        $this->assertClassHasAttribute('tranches', Loan::class);
    }

    public function testCheckConstructor()
    {
        $this->guid
            ->method('generateGuid')
            ->willReturn("guid_1");
        $loan = new Loan($this->guid, $this->startDate, $this->endDate);
        $this->assertEquals("guid_1", $loan->getGuid());
        $this->assertEquals($this->startDate, $loan->getStartDate());
        $this->assertEquals($this->endDate, $loan->getEndDate());
        $this->assertEquals([], $loan->getTranches());
    }

    public function testCanAddTrancheToLoan()
    {
        $loan = new Loan($this->guid, $this->startDate, $this->endDate);
        $this->tranche
            ->method('getGuid')
            ->willReturn("tranche_1");
        $loan->addTranche($this->tranche);
        $this->assertEquals(1, count($loan->getTranches()));
        $this->assertEquals("tranche_1", $loan->getTranches()[0]);
    }
}